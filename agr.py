from django.utils import timezone
from invoices.models import *


tz1 = timezone.now()
tz2 = tz1.replace(year=tz1.year+1)
dp1 = Department.objects.get(id=1)
a1 = Agreement(name="fk1", start_date=tz1, end_date=tz2, department=dp1)
a1.save()
