from django.contrib import admin
from aua.models import Question, Choice


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]

# class QuestionAdmin(admin.ModelAdmin):
#        fields = ['pub_date', 'question_text']

#   admin.site.register(Question)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)
