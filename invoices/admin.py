from django.contrib import admin
from invoices.models import Department, Invoice, Agreement

admin.site.register(Department)
admin.site.register(Invoice)
admin.site.register(Agreement)
