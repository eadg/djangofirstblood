from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
# from django.http import HttpResponseRedirect
# from invoices.models import *
from invoices.models import Invoice, Department, Agreement
from django.contrib import auth
from django.core.context_processors import csrf
from django.db.models import Sum


# def index(request):
#    list_department = Department.objects.all()
#    output = " ".join([p.name for p in list_department])

def index(request):
    list_department = Department.objects.all()
    context = {'list_department': list_department}
    return render(request, 'invoices/index.html', context)


def detail(request, department_id):
    department = get_object_or_404(Department, pk=department_id)
    return render(request, 'invoices/detail.html', {'department': department})


def results(request, input_d):
    response = "Results is %s"
    return HttpResponse(response % input_d)


def login(request):
    c = {}
    c.update(csrf(request))
    return render(request, 'invoices/login.html', c)


def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    tup = (username, password)
    print(" ".join(tup))
    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)
        return redirect('loggedin')
#       return HttpResponseRedirect('/faktury/accounts/loggedin')
    else:
        return redirect('invalid')


def loggedin(request):
    depart_name = request.user.username
    list_invoi = Invoice.objects.filter(
        agreement__department__name=depart_name).order_by('agreement__name')
    maps = dict()
    for ele in Agreement.objects.annotate(suma=Sum('invoice__gross')).filter(
            department__name=depart_name).order_by('name'):
        maps[ele] = Invoice.objects.filter(agreement=ele)
    dic = {'full_name': request.user.username,
           'list_invoi': list_invoi, 'maps': maps}
    return render(request, 'invoices/loggedin.html', dic)


def logout(request):
    dic = {'full_name': request.user.username}
    auth.logout(request)
    return render(request, 'invoices/logout.html', dic)


def invalid(request):
    return render(request, 'invoices/invalid.html')
