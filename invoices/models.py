from django.db import models


class Department(models.Model):
    """Name of department"""
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Agreement(models.Model):
    """Describe Agreement"""
    name = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField()
    department = models.ForeignKey(Department)
    description = models.CharField(max_length=100)
    total_gross = models.DecimalField(max_digits=12, decimal_places=2)

    def __str__(self):
        return self.name


class Invoice(models.Model):
    """Describe Invoice"""
    name = models.CharField(max_length=100)
    agreement = models.ForeignKey(Agreement)
    pay_date = models.DateField()
    gross = models.DecimalField(max_digits=11, decimal_places=2)
    description = models.CharField(max_length=100)

    def __str__(self):
        return self.name
