from django.conf.urls import patterns, url
from invoices import views

# urlpatterns = patterns('', url(r'^$', views.index, name='index'),)
urlpatterns = patterns('',
                       url(r'^$', views.login, name='login'),
#                      url(r'^$', views.index, name='index'),
                       url(r'^(?P<department_id>\d+)/$',
                           views.detail, name='detail'),
                       url(r'^(?P<input_d>\d+)/results/$',
                           views.results, name='results'),
                       url(r'^login/$',
                           views.login, name='login'),
                       url(r'^auth/$',
                           views.auth_view, name='authv'),
                       url(r'^logout/$',
                           views.logout, name='logout'),
#                      url(r'^loggedin/$',
#                          views.index, name='index'),
                      url(r'^loggedin/$',
                          views.loggedin, name='loggedin'),
                       url(r'^invalid/$',
                           views.invalid, name='invalid'),)
